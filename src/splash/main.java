package splash;

import view.splash;
/**
 *
 * @author Daniel Mendez
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        splash sp = new splash();
        sp.setVisible(true);
        
        try{
            for(int i=0; i<=100; i++){
                //Basicly it's the cycle time
                Thread.sleep(30);
                
                //This set the value in the label
                sp.number.setText(Integer.toString(i)+"%");
            }
        }catch(Exception e){
            
        }
        
    }
    
}
